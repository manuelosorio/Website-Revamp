var gulp = require('gulp');
var shell = require('gulp-shell');
var browserSync = require('browser-sync').create();
var imagemin = require('gulp-imagemin');
var fs = require('fs');
var path = require('path');
// Task for building blog when something changed:
gulp.task('build', shell.task(['bundle exec jekyll build --watch']));
// Or if you don't use bundle:
// gulp.task('build', shell.task(['jekyll build --watch']));

// let paths ={
//   styles: {
//       src: "assets/css/**/*.{sass,scss}",
//       dest: "_dist/css"
//   },
//   fonts: {
//       src: "assets/fonts/**",
//       dest: "_dist/fonts"
//   },
//   html: {
//     src: "**/*.pug",
//     exclude: "!includes/*.pug",
//     dest: "_dist/"
//   },
//   images: {
//     src: "assets/images/**/*.{jpg,jpeg,png,svg}",
//     dest: "_dist/images"
//   },
//   scripts: {
//     src: "assets/scripts/**/*.js",
//     dest: "_dist/scripts"
//   },
//   fonts: {
//     src: "assets/fonts/**",
//     dest: "_dist/fonts"
//   }
// }



// Task for serving blog with Browsersync
function serve() {
    const content_404 = fs.readFileSync(path.join(__dirname,'_site/404.html'))
    browserSync.init({
      server: {baseDir: '_site/'},
      open: false,
      port: 8080,
      callbacks: {
        ready: function(err, bs) {
          console.log(bs.options.get('urls'));
          bs.addMiddleware("*", function (req, res) {
            res.writeHead(302, {
              location: "/404.html"
            })
            res.end()
          })
        }
      }
    });
    // Reloads page when some of the already built files changed:
    gulp.watch('_site/**/*.*').on('change', browserSync.reload);
}
// Compress Images
function images() {
  gulp.src('assets/img/**/**/*')
    .pipe(imagemin({
      interlaced: true,
      progressive: true,
      optimizationLevel: 8
  }))
    .pipe(gulp.dest('_site/assets/img/'))
    .pipe(browserSync.stream());
}
exports.images = images;
exports.serve = serve;
let build =  gulp.parallel(['build', serve]) ;
gulp.task('default', build);
